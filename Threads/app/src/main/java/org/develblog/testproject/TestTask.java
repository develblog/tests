package org.develblog.testproject;

import android.os.AsyncTask;
import android.widget.TextView;

public class TestTask extends AsyncTask<Void, Void, String> {

    private TextView resultView;

    public TestTask(TextView resultView) {
        this.resultView = resultView;
    }

    @Override
    protected String doInBackground(Void... voids) {
        // Berechnungen simulieren
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
        }

        return "Async Task beendet";
    }

    @Override
    protected void onPostExecute(String s) {
        resultView.setText(s);
    }
}
