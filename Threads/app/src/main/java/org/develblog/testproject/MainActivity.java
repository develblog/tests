package org.develblog.testproject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    Unbinder unbinder;
    ProgressBarHandler handler;

    /**
     * Views.
     */
    @BindView(R.id.resultView)
    TextView resultView;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @OnClick(R.id.handler1Button)
    public void startProgressBarMessageThread() {
        if (handler == null) {
            handler = new ProgressBarHandler(this.getMainLooper());
        }
        resultView.setText("Start Message Thread");
        progressBar.setProgress(0);

        new ProgressBarMessageThread(handler).start();
    }

    @OnClick(R.id.handler2Button)
    public void startProgressBarThread() {
        progressBar.setProgress(0);
        resultView.setText("Start Thread");
        new ProgressBarThread(resultView, progressBar).start();
    }

    @OnClick(R.id.asyncButton)
    public void startAsyncTask() {
        resultView.setText("Start Async Task");
        new TestTask(resultView).execute();
    }

    private class ProgressBarHandler extends Handler {

        public ProgressBarHandler(Looper mainLooper) {
            super(mainLooper);
        }

        @Override
        public void handleMessage(Message msg) {
            int count = msg.getData().getInt("count");

            resultView.setText("Durchlauf: " + count);
            progressBar.incrementProgressBy(10);
        }
    }
}
