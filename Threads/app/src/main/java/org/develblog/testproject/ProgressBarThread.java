package org.develblog.testproject;

import android.widget.ProgressBar;
import android.widget.TextView;

public class ProgressBarThread extends Thread {

    private TextView resultView;
    private ProgressBar progressBar;

    public ProgressBarThread(TextView resultView, ProgressBar progressBar) {
        this.resultView = resultView;
        this.progressBar = progressBar;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10; i++) {
            // Berechnungen simulieren
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            final int count = i;

            progressBar.post(new Runnable() {
                @Override
                public void run() {
                    resultView.setText("Durchlauf: " + count);
                    progressBar.incrementProgressBy(10);
                }
            });
        }
    }
}
