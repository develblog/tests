package org.develblog.testproject;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class ProgressBarMessageThread extends Thread {

    private Handler handler;

    public ProgressBarMessageThread(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void run() {
        for (int i = 1; i <= 10; i++) {
            // Berechnungen simulieren
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            Bundle b = new Bundle();
            b.putInt("count", i);

            Message m = new Message();
            m.setData(b);

            handler.sendMessage(m);
        }
    }
}
