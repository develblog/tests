package org.develblog.testproject.dagger.module;

import android.content.Context;

import org.develblog.testproject.dagger.qualifier.ApplicationContext;
import org.develblog.testproject.dagger.scope.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Context context;

    public AppModule(@ApplicationContext Context context) {
        this.context = context;
    }

    @Provides
    @ApplicationScope
    @ApplicationContext
    Context provideContext() {
        return context;
    }
}
