package org.develblog.testproject.dagger.module;

import android.content.Context;

import org.develblog.testproject.dagger.qualifier.MainActivityContext;
import org.develblog.testproject.dagger.scope.MainActivityScope;
import org.develblog.testproject.dependency.activity.ActivityDependency1;
import org.develblog.testproject.dependency.activity.ActivityDependency2;
import org.develblog.testproject.dependency.application.AppDependency1;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Context context;

    public ActivityModule(@MainActivityContext Context context) {
        this.context = context;
    }

    @Provides
    @MainActivityScope
    @MainActivityContext
    Context provideContext() {
        return context;
    }

    @Provides
    @MainActivityScope
    ActivityDependency1 provideActivityDependency1() {
        return new ActivityDependency1();
    }

    @Provides
    @MainActivityScope
    ActivityDependency2 proActivityDependency2(AppDependency1 appDependency1, ActivityDependency1 activityDependency1) {
        return new ActivityDependency2(appDependency1, activityDependency1);
    }

}
