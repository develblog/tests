package org.develblog.testproject;

import org.develblog.testproject.dagger.component.AppComponent;
import org.develblog.testproject.dagger.component.DaggerAppComponent;
import org.develblog.testproject.dagger.module.AppModule;

import timber.log.Timber;

public class TestApplication extends android.app.Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        // init log
        Timber.plant(new Timber.DebugTree());

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        Timber.d("appDependency2: %s", appComponent.appDependency2());
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
