package org.develblog.testproject.dagger.module;


import android.content.Context;

import org.develblog.testproject.dagger.qualifier.ApplicationContext;
import org.develblog.testproject.dagger.scope.ApplicationScope;
import org.develblog.testproject.dependency.application.AppDependency1;
import org.develblog.testproject.dependency.application.AppDependency2;

import dagger.Module;
import dagger.Provides;

@Module
public class NetModule {

    @Provides
    @ApplicationScope
    AppDependency1 provideAppDependency1() {
        return new AppDependency1();
    }

    @Provides
    @ApplicationScope
    AppDependency2 provideAppDependency2(@ApplicationContext Context context, AppDependency1 appDependency1) {
        return new AppDependency2(context, appDependency1);
    }
}
