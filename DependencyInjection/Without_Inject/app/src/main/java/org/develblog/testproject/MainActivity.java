package org.develblog.testproject;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.develblog.testproject.dagger.component.ActivityComponent;
import org.develblog.testproject.dagger.component.AppComponent;
import org.develblog.testproject.dagger.component.DaggerActivityComponent;
import org.develblog.testproject.dagger.module.ActivityModule;
import org.develblog.testproject.dependency.activity.ActivityDependency1;
import org.develblog.testproject.dependency.activity.ActivityDependency2;
import org.develblog.testproject.dependency.application.AppDependency2;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    AppDependency2 appDependency2;
    ActivityDependency1 activityDependency1;
    ActivityDependency2 activityDependency2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppComponent appComponent = ((TestApplication) getApplication()).getAppComponent();
        ActivityComponent activityComponent = DaggerActivityComponent.builder()
                .appComponent(appComponent)
                .activityModule(new ActivityModule(this))
                .build();

        appDependency2 = appComponent.appDependency2();
        activityDependency1 = activityComponent.activityDependency1();
        activityDependency2 = activityComponent.activityDependency2();

        Timber.d("appDependency2: %s", appDependency2);
        Timber.d("activityDependency1: %s", activityDependency1);
        Timber.d("activityDependency2: %s", activityDependency2);
    }
}
