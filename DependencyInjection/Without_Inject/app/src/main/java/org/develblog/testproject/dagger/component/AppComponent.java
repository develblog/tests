package org.develblog.testproject.dagger.component;


import android.content.Context;

import org.develblog.testproject.dagger.module.AppModule;
import org.develblog.testproject.dagger.module.NetModule;
import org.develblog.testproject.dagger.qualifier.ApplicationContext;
import org.develblog.testproject.dagger.scope.ApplicationScope;
import org.develblog.testproject.dependency.application.AppDependency1;
import org.develblog.testproject.dependency.application.AppDependency2;

import dagger.Component;

@ApplicationScope
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {

    @ApplicationContext
    Context context();

    AppDependency1 appDependency1();

    AppDependency2 appDependency2();

}
