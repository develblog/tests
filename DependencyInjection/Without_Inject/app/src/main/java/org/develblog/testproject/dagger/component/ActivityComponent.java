package org.develblog.testproject.dagger.component;


import android.content.Context;

import org.develblog.testproject.dagger.module.ActivityModule;
import org.develblog.testproject.dagger.qualifier.MainActivityContext;
import org.develblog.testproject.dagger.scope.MainActivityScope;
import org.develblog.testproject.dependency.activity.ActivityDependency1;
import org.develblog.testproject.dependency.activity.ActivityDependency2;

import dagger.Component;

@MainActivityScope
@Component(dependencies = {AppComponent.class}, modules = {ActivityModule.class})
public interface ActivityComponent {

    @MainActivityContext
    Context context();

    ActivityDependency1 activityDependency1();

    ActivityDependency2 activityDependency2();

}
