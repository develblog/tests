package org.develblog.testproject;

import org.develblog.testproject.dagger.component.AppComponent;
import org.develblog.testproject.dagger.component.DaggerAppComponent;
import org.develblog.testproject.dagger.module.AppModule;
import org.develblog.testproject.dependency.application.AppDependency2;

import javax.inject.Inject;

import timber.log.Timber;

public class TestApplication extends android.app.Application {

    private AppComponent appComponent;

    @Inject
    AppDependency2 appDependency2;

    @Override
    public void onCreate() {
        super.onCreate();

        // init log
        Timber.plant(new Timber.DebugTree());

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        appComponent.inject(this);

        Timber.d("appDependency2: %s", appDependency2);
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
