package org.develblog.testproject.dagger.module;

import android.content.Context;

import org.develblog.testproject.dagger.qualifier.MainActivityContext;
import org.develblog.testproject.dagger.scope.MainActivityScope;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Context context;

    public ActivityModule(@MainActivityContext Context context) {
        this.context = context;
    }

    @Provides
    @MainActivityScope
    @MainActivityContext
    Context provideContext() {
        return context;
    }

}
