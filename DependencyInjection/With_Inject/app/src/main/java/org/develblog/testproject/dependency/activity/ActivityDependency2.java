package org.develblog.testproject.dependency.activity;

import org.develblog.testproject.dagger.scope.MainActivityScope;
import org.develblog.testproject.dependency.application.AppDependency1;

import javax.inject.Inject;

@MainActivityScope
public class ActivityDependency2 {

    @Inject
    public ActivityDependency2(AppDependency1 appDependency1, ActivityDependency1 activityDependency1) {
    }

}
