package org.develblog.testproject.dependency.activity;

import org.develblog.testproject.dagger.scope.MainActivityScope;

import javax.inject.Inject;

@MainActivityScope
public class ActivityDependency1 {

    @Inject
    public ActivityDependency1() {
    }
}
