package org.develblog.testproject.dependency.application;

import org.develblog.testproject.dagger.scope.ApplicationScope;

import javax.inject.Inject;

@ApplicationScope
public class AppDependency1 {

    @Inject
    public AppDependency1() {
    }
}
