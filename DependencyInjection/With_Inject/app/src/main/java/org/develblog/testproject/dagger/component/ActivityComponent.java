package org.develblog.testproject.dagger.component;


import org.develblog.testproject.MainActivity;
import org.develblog.testproject.dagger.module.ActivityModule;
import org.develblog.testproject.dagger.scope.MainActivityScope;

import dagger.Component;

@MainActivityScope
@Component(dependencies = {AppComponent.class}, modules = {ActivityModule.class})
public interface ActivityComponent {

    void inject(MainActivity mainActivity);

}
