package org.develblog.testproject.dependency.application;

import android.content.Context;

import org.develblog.testproject.dagger.qualifier.ApplicationContext;
import org.develblog.testproject.dagger.scope.ApplicationScope;

import javax.inject.Inject;

@ApplicationScope
public class AppDependency2 {

    @Inject
    public AppDependency2(@ApplicationContext Context context, AppDependency1 appDependency1) {
    }

}
