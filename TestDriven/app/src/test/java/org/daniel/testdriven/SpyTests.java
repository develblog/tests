package org.daniel.testdriven;

import org.junit.Before;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class SpyTests {

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @org.junit.Test
    public void addUser() {
        UserBackend backend = new UserBackend();

        User user = new User("daniel", "geheim");
        User spyUser = spy(user);

        when(spyUser.getPassword()).thenReturn(null);

        assertTrue(backend.addUser(user));
        assertFalse(backend.addUser(spyUser));
    }

    @org.junit.Test
    public void lowerText() {
        Test spyTest = spy(new Test());
        assertEquals(spyTest.lowerText("Test"), "test");

        when(spyTest.lowerText(any(String.class))).then(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return args[0].toString().toUpperCase();
            }
        });
        assertEquals(spyTest.lowerText("Test"), "TEST");
    }

    public class Test {
        public String lowerText(String text) {
            if (text != null) {
                return text.toLowerCase();
            }

            return "";
        }
    }
}