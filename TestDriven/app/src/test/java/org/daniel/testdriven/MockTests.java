package org.daniel.testdriven;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class MockTests {

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addUser() {
        UserBackend backend = new UserBackend();

        User user1 = mock(User.class);
        when(user1.getUsername()).thenReturn("daniel");
        when(user1.getPassword()).thenReturn("geheim");

        assertTrue(backend.addUser(user1));

        //--------------------------------------------------
        User user2 = mock(User.class);
        when(user2.getUsername()).thenReturn(null);
        when(user2.getPassword()).thenReturn("geheim");

        assertFalse(backend.addUser(user2));

        //------------------------------------------------------
        User user3 = mock(User.class);
        when(user3.getUsername()).thenReturn("daniel");
        when(user3.getPassword()).thenReturn(null);

        assertFalse(backend.addUser(user3));
    }
}