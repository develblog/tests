package org.daniel.testdriven;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by dast2de on 24.11.2016.
 */

public class AssertTests {

    @Test
    public void createUser() {
        String username = "daniel";
        String password = "geheim";

        User user = new User(username, password);
        user.setUsername(username);
        user.setPassword(password);

        assertEquals(username, user.getUsername());
        assertEquals(password, user.getPassword());
    }
}
