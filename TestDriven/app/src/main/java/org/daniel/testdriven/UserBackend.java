package org.daniel.testdriven;

/**
 * Created by dast2de on 24.11.2016.
 */

public class UserBackend {

    public Boolean addUser(User user) {
        return user.getUsername() != null && user.getPassword() != null;
    }
}
