package org.daniel.test;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class MainActivity extends AppCompatActivity {

    Unbinder binder;
    @BindView(R.id.infoView)
    TextView infoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binder = ButterKnife.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        binder.unbind();
    }

    @OnClick(R.id.button)
    public void readPerson() {
        new LoadDataTask().execute(new String[]{"https://api.github.com/repos/square/okhttp/contributors"});
    }

    private class LoadDataTask extends AsyncTask<String, Void, String> {

        private List<Data> dataList;

        protected String doInBackground(String... urls) {
            try {
                OkHttpClient client = new OkHttpClient();

                // Create request for remote resource.
                Request request = new Request.Builder()
                        .url(urls[0])
                        .build();

                // Execute the request and retrieve the response.
                Response response = client.newCall(request).execute();

                // Deserialize HTTP response to concrete type.
                ResponseBody body = response.body();

                dataList = new ObjectMapper().readValue(body.string(), new TypeReference<List<Data>>() {
                });
                body.close();

                if (!dataList.isEmpty()) {
                    return dataList.get(0).getLogin();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "Does not work";
        }

        @Override
        protected void onPostExecute(String login) {
            infoView.setText(login);
        }
    }
}
