package org.daniel.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by dast2de on 02.12.2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("login")
    private String login;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
